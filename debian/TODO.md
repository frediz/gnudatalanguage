GNU Data Language Debian Packaging TODO
=======================================

Split up in separate packages
-----------------------------

* `gnudatalanguage-nox`: Package without X dependencies, see
  https://bugs.debian.org/753618
* `gnudatalanguage-doc`: LaTeX-written documentation under
  `doc/udg`. Needs at least the following build-dependencies:
  * `texlive-latex-extra`: `forloop`, `stringstrings`, `authorindex`
  * Not yet available:
    * `pdfdraftcopy.sty` from
      http://sarovar.org/projects/pdfdraftcopy/
    * `nnfootnote.sty` from http://ctan.org/pkg/nnfootnote/
  This is currently impossible since doc/ disappeared in source tarball
